export type Game = {
  id: string;
  date: string;
  time: string;
  home: string;
  away: string;
  year: number;
  homeScore: number | null;
  awayScore: number | null;
  practice: boolean;
  rain: boolean;
};

export type PreGame = {
  date: string;
  time: string;
  home: string;
  away: string;
};
