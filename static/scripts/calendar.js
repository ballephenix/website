import * as util from "./util.js";
import m from "https://esm.sh/mithril@2.2.2"
import _ from "https://esm.sh/lodash-es@4.17.21"
import {statsView, tabView, schedulView } from './schedule.js'

const calendarPage = {
  view: function(){
    const availableYears = util.getAvailableYears();
    const year = _.get(m.route.param(), 'year', util.getDefaultYear());

    return  m('div.uk-grid.uk-margin-top.uk-margin-bottom',
      m('div.uk-width-large-3-5.uk-width-medium-4-5.uk-width-small-1-1.uk-container-center',
        [
          m(statsView, {year}),
          m(tabView, {year, availableYears}),
          m(schedulView, {year}),
        ]),
      )
  }
}

m.route.mode = "hash";
m.route(document.getElementById('mithril-content'), "/", {
      "/": calendarPage,
      "/:year": calendarPage,
});
