import * as util from "./util.js";
import * as resultParser from './resultParser.js'
import * as teams from './teams.js'
import m from "https://esm.sh/mithril@2.2.2"
import {rainSvg} from "./images.js";
import _ from "https://esm.sh/lodash-es@4.17.21"

function ppYear(year, availableYears) {
  return _.last(availableYears) === year ? "" : year;
}


export const scheduleTeamTags = {};

scheduleTeamTags._getTeamTag = function(teamName){
    var teamColor = teams.getTeamColor(teamName);
    return [
        m('i.fa.fa-flag', {style: {color: teamColor}}),
        m('span', ' ' + teamName)
    ];
};

scheduleTeamTags._getAwayPracticeTeamTag = function(){
    return  [
        m('i.fa.fa-hand-spock-o', {style: {color: ' #ff1493'}, title: 'pratique'}),
        m('span', ' Amis #1')
    ];
};

scheduleTeamTags._getHomePracticeTeamTag = function(){
    return  [
        m('i.fa.fa-hand-spock-o', {style: {color: '#acbf60'}, title: 'pratique'}),
        m('span', ' Amis #2')
    ];
};

scheduleTeamTags.getAwayTeamTag = function(game){
    if(game.practice){
        return scheduleTeamTags._getAwayPracticeTeamTag();
    }
    return scheduleTeamTags._getTeamTag(game.away);
};

scheduleTeamTags.getHomeTeamTag = function(game){
    if(game.practice){
        return scheduleTeamTags._getHomePracticeTeamTag();
    }
    return scheduleTeamTags._getTeamTag(game.home);
};



export const statsView = {};
statsView.stats = [];

statsView.oninit = async function(vnode){
  if(statsView.currentYear === vnode.attrs.year){
    return;
  }
  statsView.currentYear = vnode.attrs.year;
  const result = await  m.request({
             method: "GET",
             url: `/api/results/${vnode.attrs.year}.json`
         });
  statsView.stats = resultParser.getStats(result)
  return result;
};

statsView.onupdate = statsView.oninit;


statsView.getStatRow = function(teamStats){
    var formatStat = function(n){
        return teamStats.gamePlayed === 0 ? '-' : n;
    };
    return m('tr', [
        m('td', teamStats.team),
        m('td', teamStats.gamePlayed),
        m('td', formatStat(teamStats.victories)),
        m('td', formatStat(teamStats.defeats)),
        m('td', formatStat(teamStats.draws)),
        m('td', formatStat(teamStats.pointsFor)),
        m('td', formatStat(teamStats.pointsAgaint)),
        m('td', formatStat(teamStats.pointsFor - teamStats.pointsAgaint)),
        m('td', formatStat(teamStats.points))
    ]);
};

statsView.isStatForValidTeam = function(stat){
    var teamNames = _.map(teams.teams(), 'name');
    return _.includes(teamNames, stat.team);
}


statsView.mainTable = function(stats){
    var orderedStats = _.reverse(_.sortBy(stats, ['points', 'pointsFor']));
    return m('table.uk-table.uk-table-hover.uk-table-striped',[
        m('thead', m('tr', [
            m('th', 'Équipes'),
            m('th', 'PJ'),
            m('th', 'V'),
            m('th', 'D'),
            m('th', 'N'),
            m('th', 'PP'),
            m('th', 'PC'),
            m('th', 'DIF'),
            m('th', 'PTS')
        ])),
        m('tbody', _(orderedStats)
                    .filter(statsView.isStatForValidTeam)
                    .map(statsView.getStatRow)
                    .value())
    ]);
};

statsView.legend = function(){
  var items = ["PJ : Parties joués", "V : Victoire",  "D : Défaite",
                "N : Nul", "PP : Points pour", "PC : Points contre",
                "DIF : Diférenciel","PTS : Points"];
  return  m('ul.uk-subnav.uk-subnav-line.uk-flex-center',[
              _.map(items, function(itm){
                  return m('li', itm);
              })
          ]);
};

statsView.view = function(vnode){
  return  m('div.uk-grid',
              m('div.uk-width-medium-1-1.uk-width-large-1-1.uk-container-center',[
                  m('h2', 'Classement ' + vnode.attrs.year),
                  statsView.mainTable(statsView.stats),
                  statsView.legend()
              ]));

};



export const schedulView = {};

schedulView.oninit = async function(vnode){
  if(vnode.attrs.year === schedulView.currentYear){
      return;
  }
  schedulView.currentYear = vnode.attrs.year;
  const result = await  m.request({
             method: "GET",
             url: `/api/results/${vnode.attrs.year}.json`
         });
  schedulView.games = resultParser.getNightGames(result)

};

schedulView.onupdate =  schedulView.oninit;


schedulView._asGamePoints = function(points){
    return !_.isNumber(points) ? '-' : points + '';
};

schedulView._rainIcon = function(game){
    if(game.rain){
        return m('span', {title: 'pluie, partie annulée.'}, rainSvg('1.5em'));
    }
    return m('span', {style: {visibility: 'hidden'}}, rainSvg('1.5em'));
};

schedulView._gameName = function(game){
    return [schedulView._rainIcon(game),
            " ",
            game.time];
};

schedulView._gameRow = function(game){

    return m('tr',[
        m('td', schedulView._gameName(game)),
        m('td', scheduleTeamTags.getAwayTeamTag(game)),
        m('td', schedulView._asGamePoints(game.away_points)),
        m('td', 'vs'),
        m('td', scheduleTeamTags.getHomeTeamTag(game)),
        m('td', schedulView._asGamePoints(game.home_points))
    ]);
};

schedulView._gameView = function(gameNight){
    return m('div', [m('h3', util.formatDate(gameNight.jsDate)),
        m('table.uk-table.uk-table-hover.uk-table-condensed.uk-table-striped',
            m('tbody',[
                _.map(gameNight.games, schedulView._gameRow),
            ])
        )
    ]);

};

schedulView.view = function(){
    return m('div.uk-grid',
              m('div.uk-width-large-3-5.uk-width-medium-1-1',
                _.map(schedulView.games, schedulView._gameView)));
};


export const tabView = {};

tabView._createTab = function(activeYear, availableYears, year){
  const isActive = (activeYear == year) || (activeYear == ppYear(year, availableYears));
  return m('li', {'class': isActive ? 'uk-active': ''}, //  'uk-active'
      m('a', {href: '#/' + ppYear(year, availableYears)}, 'Saison ' + year));
}

tabView.view = function(vnode){
    const activeYear = vnode.attrs.year;
    const availableYears = vnode.attrs.availableYears;
    const createTab = _.partial(tabView._createTab, activeYear, availableYears);
    return m('div.uk-margin', m('h2', 'Calendrier ' +  ppYear(activeYear, availableYears)),
        m('ul.uk-tab', _.reverse(_.map(availableYears, createTab))));
};
