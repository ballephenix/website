{
  description = "Phenix website";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:

    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        build-static-files = pkgs.stdenv.mkDerivation {
          name = "phenix-website-static-assets";
          src = ./static;
          buildPhase = ''
            find . -type f -exec ${pkgs.brotli}/bin/brotli -9 -f -k {} \;
          '';
          installPhase = ''
            cp -r . $out

          '';
        };
      in {
        packages = { static = build-static-files; };
        devShell = pkgs.mkShell {
          name = "Phenix-Website";
          buildInputs = with pkgs; [ deno ];
        };
      });
}
