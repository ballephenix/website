import * as _ from "https://esm.sh/lodash-es@4.17.21"


let  resultParser = {};

resultParser._addJSDateToResult = function(res){
    let  date = res.date.split('-');
    let  time = _.isString(res.time) ? res.time.split(':'): [0, 0];
    res.jsDate = new Date(
        Number(date[0]),
        Number(date[1]) - 1,
        Number(date[2]),
        Number(time[0]),
        Number(time[1])
    );
    return res;
};

resultParser._setCanonicalPoints = function(res){
  if(!_.isNumber(res.home_points) && _.isNumber(_.get(res, res.home))){
    res.home_points = res[res.home];
  }
  if(!_.isNumber(res.away_points) && _.isNumber(_.get(res, res.away))){
    res.away_points = res[res.away];
  }
  return res;
}

resultParser.parseResults = function(rawResults){
    rawResults = _.map(rawResults, resultParser._setCanonicalPoints);
    rawResults = _.map(rawResults, resultParser._addJSDateToResult);
    rawResults = _.map(rawResults, resultParser._addCustomStyle);
    return rawResults;
};

resultParser.getNightGames = function(rawResults){
    let  ret = {};
    _.each(resultParser.parseResults(rawResults), function(res){
        if(_.has(ret, res.date)){
            ret[res.date].push(res);
        }else{
            ret[res.date] = [res];
        }
    });
    return _.map(ret, function(games, date){
        return resultParser._addJSDateToResult({date: date, games: games});
    });
};

resultParser._getAllTeams = function(rawResults){
    let  homeTeams = _.map(rawResults, 'home');
    let  awayTeams = _.map(rawResults, 'away');
    return _.uniq(_.concat(homeTeams, awayTeams));
};

resultParser._getWinner = function(res){
    if(res.home_points > res.away_points){
        return res.home;
    }
    if(res.home_points < res.away_points){
        return res.away;
    }
};

resultParser._hasWon = function(res, team){
    return resultParser._getWinner(res) == team;
};

resultParser._hasLost = function(res, team){
    return !resultParser._isDraw(res) && resultParser._getWinner(res) != team;
};

resultParser._isDraw = function(res){
    return res.home_points == res.away_points;
};

resultParser._getPointsFor = function(res, team){
    if(res.home == team){
        return res.home_points;
    }
    return res.away_points;
};

resultParser._getPointsForOpponent = function(res, team){
    if(res.home == team){
        return res.away_points;
    }
    return res.home_points;
};

resultParser._getPointsForGame = function(res, team){
    if(resultParser._hasWon(res, team)){
        return 2;
    }
    return resultParser._isDraw(res) ? 1: 0;
};

resultParser._getTeamStats = function(rawResults, team){
    let  ret = {
        team: team, gamePlayed: 0, victories: 0, points: 0,
        defeats: 0, draws: 0, pointsFor: 0, pointsAgaint: 0};
    _.each(rawResults, function(res){
        if((team != res.home && team != res.away) || !_.isNumber(res.home_points)){
            return;
        }
        ret.gamePlayed += 1;
        ret.victories += resultParser._hasWon(res, team) ? 1: 0;
        ret.defeats += resultParser._hasLost(res, team) ? 1: 0;
        ret.draws += resultParser._isDraw(res) ? 1: 0;
        ret.pointsFor += resultParser._getPointsFor(res, team);
        ret.pointsAgaint += resultParser._getPointsForOpponent(res, team);
        ret.points += resultParser._getPointsForGame(res, team);
    });
    return ret;
};

resultParser._removeTemporaryResults = function(rawResults){
    return _.filter(rawResults, function(rawResult){
        return !rawResult.temporary;
    });
};

resultParser._isValidGame = function(game){
    return !game.rain && ! game.practice;
};

resultParser.getStats = function(rawResults){
    rawResults = _.filter(rawResults, resultParser._isValidGame);
    rawResults = _.map(rawResults, resultParser._setCanonicalPoints);
    let allTeams = resultParser._getAllTeams(resultParser._removeTemporaryResults(rawResults));
    let getTeamStats = _.partial(resultParser._getTeamStats, rawResults);
    return _.map(allTeams, getTeamStats);
};

export const getStats = resultParser.getStats;
export const getNightGames = resultParser.getNightGames;
