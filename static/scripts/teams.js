import * as _ from "https://esm.sh/lodash-es@4.17.21"


export function teams(){
    return [
        {name: 'remplaçants', color: '#FFA500'},
        {name: 'jaune', color: '#FF0'},
        {name:'rouge', color: '#F00'},
        {name:'bleu', color: '#00F'},
        {name:'gris', color: '#808080'},
        {name:'vert', color: '#008000'},
        {name: 'noir', color: '#000000'},
        {name: 'orange', color: '#FFA500'},
        {name: 'mauve', color: '#E0B0FF'}
    ];
};

export function getTeamColor(teamName){
    return teams().filter(t => t.name == teamName)[0]?.color;
};
