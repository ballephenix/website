// alert("ok")
import * as util from "./util.js";
import * as resultParser from './resultParser.js'
import * as teams from './teams.js'
import m from "https://esm.sh/mithril@2.2.2"
import {rainSvg} from "./images.js";
import _ from "https://esm.sh/lodash-es@4.17.21"

import {statsView, tabView, scheduleTeamTags } from './schedule.js'


const nextGameDaySchedule = {};

nextGameDaySchedule._toDay = function(){
    var now = new Date();
    return new Date(now.getFullYear(), now.getMonth(), now.getDate());
};

nextGameDaySchedule.oninit = async function(){

  const result = await  m.request({
             method: "GET",
             url: `/api/results/2024.json`
         });
  const nightGames = resultParser.getNightGames(result);
  nextGameDaySchedule.games = nextGameDaySchedule._getNextNightGame(nightGames, nextGameDaySchedule._toDay());
};

nextGameDaySchedule._getNextNightGame = function(games, date){
    return _(games)
      .filter(g =>  g.jsDate.getTime() >= date.getTime())
      .last()
      .games
      .sort((g1, g2) =>  g1.jsDate -  g2.jsDate);
};

nextGameDaySchedule._getTableHead = function(){
    return m('thead', m('tr', [
                m('td', 'Date'),
                m('td', 'Heure'),
                m('td', 'Receveur'),
                m('td', 'Visiteur')
            ]));
};

nextGameDaySchedule._responsoveText = function(largeText, smallText, game){
    var ret = [];
    if(game.rain){
        ret.push(rainSvg('1.5em'));
        ret.push(' ');
    } else if(game.practice){
        ret.push(m('span.uk-badge.uk-badge-success', 'pratique!'));
        ret.push(' ');
    }
    ret.push(m('span.uk-visible-large', largeText));
    ret.push(m('span.uk-hidden-large', smallText));
    return ret;
};

nextGameDaySchedule._getGamesRow = function(game){
    return m('tr', [
        m('td', nextGameDaySchedule._responsoveText(util.formatDate(game.jsDate),
                                                    util.formatDateSmall(game.jsDate),
                                                    game)),
        m('td', game.time),
        m('td', scheduleTeamTags.getHomeTeamTag(game)),
        m('td',scheduleTeamTags.getAwayTeamTag(game))
    ]);
};

nextGameDaySchedule._getGamesRows = function(games){
    return _.map(games, nextGameDaySchedule._getGamesRow);
};

nextGameDaySchedule.view = function(){
    return  m('table.uk-table.uk-table-hover.uk-table-striped.uk-table-condensed',[
                nextGameDaySchedule._getTableHead(),
                m('tbody',
                    nextGameDaySchedule._getGamesRows(nextGameDaySchedule.games)
                )]);
};





const LittleCalendar = {}

LittleCalendar.view = function(){
  return m(nextGameDaySchedule);
}

const containerIds = ['large-mithril-container',
  'medium-mithril-container',
  'small-mithril-container']

for(const id of containerIds){
  m.mount(document.getElementById(id), LittleCalendar);
}
