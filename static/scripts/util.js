import m from "https://esm.sh/mithril@2.2.2"
import * as _ from "https://esm.sh/lodash-es@4.17.21"


let availableYears = []
m.request({
           method: "GET",
           url: `api/available-years.json`
}).then(d => {
    availableYears = d.sort();
})


function resultatsURL (year){
    var cacheBuster = window.Date.now();
    year = !!year ? year: util.getDefaultYear();
    return "api/resultats" + year + ".json?_=" + cacheBuster;
};

export function getAvailableYears(){
    if(window.availableYears){
        return _.clone(window.availableYears)
    }
    return availableYears.map(x => x)

};

export function getDefaultYear(){
    return _.max(getAvailableYears());
}

export function formatDate(date){
    const monthNames = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin',
                        'juillet', 'août', 'septembre', 'octobre', 'novembre',
                        'decembre'];
    return [date.getDate(),
            monthNames[date.getMonth()],
            date.getFullYear()].join(' ');

};



export function getResults(year){
    var url = resultatsURL(year);
    return m.request({method: "GET", url: url})
};

export function getDefaultResults() {
    return getResults(getDefaultYear());
};


export function formatDateSmall(date){
  var smallMonthNames = ['janv', 'févr', 'mars', 'avr', 'mai', 'juin',
                           'juill', 'août', 'sept', 'oct', 'nov',
                           'dec'];
         return [date.getDate(),
                 smallMonthNames[date.getMonth()],
                 date.getFullYear() % 1000].join(' ');


}
